# i mac hittar den inte rätt git innan bash är igång, ett fulhack som funkar
unamestr=`uname`
if [[ "$unamestr" != 'Linux' ]]; then
  export PATH="/opt/local/bin:$PATH"
fi

#kollar om vi är i en repository
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

set_prompt_color() {
  if [ -z "$1" ]; then
    echo "No color is set for first part"
  fi
  if [ -z "$2" ]; then
    echo "No color is set for second part"
  fi
  PS1="\[\e[01;$1m\]\h \[\033[01;$2m\]\w \$(parse_git_branch): \[\e[00m\]"
}
# En enkel promt med git med om vi står i ett repository
PS1="No prompt settings is done in .bash_local. (set_prompt_color 31 36) : "

# Startar en bakgrundsprocess för att ladda ner uppdateringar till configure. Ev är det klart till slutet. Annars får man status till nästa login
(git --git-dir=$HOME/configure/.git --work-tree=$HOME/configure fetch &) > /dev/null 2>&1

if [ "$TERM" == "xterm" ]; then
  # No it isn't, it's gnome-terminal
  export TERM=xterm-256color
fi



# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.

# git ( en . betyder source )
if [ -f $HOME/.bash_aliases ]; then
  . $HOME/.bash_aliases
fi

export SVN_EDITOR=vi

# git ( en . betyder source )
if [ -f $HOME/configure/git/git-completion.bash ]; then
  . $HOME/configure/git/git-completion.bash
fi

# Custom kod för denna maskin
if [ -f $HOME/.bash_local ]; then
  . $HOME/.bash_local
fi

if [ ! -f $HOME/.gitconfig_local ]; then
    echo ".gitconfig_local is missing. Use it for local user info"
fi

GITBEHIND=$(git --git-dir=$HOME/configure/.git --work-tree=$HOME/configure rev-list HEAD...origin/master --count)
if [ "$GITBEHIND" != "0" ]; then
  echo
  echo "You are behind in ~/configure/.git with $GITBEHIND commits."
  git --git-dir=$HOME/configure/.git --work-tree=$HOME/configure log HEAD..origin/master --oneline
  echo "Update with: cd ~/configure; git merge origin/master; cd ..;source ~/.bashrc"
fi
