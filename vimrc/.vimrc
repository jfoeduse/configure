" För att ladda plugin
filetype plugin indent on
syntax on

" Status line
set noruler
set laststatus=2
"set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%m%r%y%=%c,%l/%L\ %P
set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}],%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
" Hur tabbar mm ska bete sig
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set nowrap
set autoindent
set smartindent

" För att visa dåliga whitespace
highlight BadWhitespace ctermbg=red guibg=red
" Visar mellanslag i slutet på rader och tab-tecken som BadWhitespace
match BadWhitespace /\s\+$\|\t/

" Tar bort mellanslag i slutet av raden när man sparar, gäller alla filer
autocmd BufWritePre * :%s/\s\+$//e

" Markerar typ på vissa filer
" :au[tocmd]
au BufNewFile,BufRead *.module set ft=php
au BufNewFile,BufRead *.conf set ft=apache
au BufNewFile,BufRead *.default set ft=php
au BufNewFile,BufRead *.md set ft=markdown

" autoformat shift-F,
noremap <S-F> gg <S-V> <S-G> =
