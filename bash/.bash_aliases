# Några lämpliga alias
unamestr=`uname`
alias ls='ls --color=auto'
if [[ "$unamestr" != 'Linux' ]]; then
  alias ls='ls -G'
fi
alias grep='grep --color=auto'
alias ll='ls -lah'
