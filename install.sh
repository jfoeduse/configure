#!/bin/bash
update_file() {
  if [ -h $HOME/$1 ]; then
    rm $HOME/$1
  fi
  if [ -f $HOME/$1 ]; then
    mv $HOME/$1 $HOME/.vimrc.old
  fi
  ln -s $HOME/configure/$2/$1 $HOME/$1
}

update_file .vimrc vimrc
update_file .bashrc bash
update_file .bash_aliases bash
update_file .gitconfig git
update_file .gitignore_global git

echo "Installing bash colors"
echo "31-red, 32-green, 33-yellow, 34-blue, 35-purple, 36-cyan, 37-white"
echo -n "Machine name color [31]:"
read color1
if [ -z "$color1" ]; then
  color1="31"
fi
if ! [[ "$color1" =~ ^[0-9]+$ ]] ; then
     exec >&2; echo "error: Not a number"; exit 1
fi

echo -n "Path color [33]:"
read color2
if [ -z "$color2" ]; then
  color2="33"
fi
if ! [[ "$color2" =~ ^[0-9]+$ ]] ; then
     exec >&2; echo "error: Not a number"; exit 1
fi

echo "set_prompt_color $color1 $color2" >> ~/.bash_local


echo -n "Git name [Ditt namn]:"
read name
if [ -z "$name" ]; then
  name="Ditt namn"
fi
echo -n "Git email [din@email.se]:"
read email
if [ -z "$email" ]; then
 email="din@email.se"
fi

echo "[user]
  name = $name
  email = $email" >> ~/.gitconfig_local

# Ladda om .bashrc
echo
echo "Configuration is saved. For updating your shell (this script has it own) run following command:
source ~/.bashrc"
